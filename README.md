Cómo rastrear el teléfono iPhone
La mejor manera de rastrear un iPhone con la última versión de iOS instalada es Where's, la función de localización que está activada en todos los dispositivos de Apple por defecto. La función te permite encontrar y localizar la posición de tu teléfono en el mapa desde un ordenador u otro iPhone, todo lo que tenemos que hacer es acceder a la página web de iCloud e introducir nuestro ID de Apple cuando perdemos o nos roban el iPhone.
Encuentra el iPhone

Una vez que hayas abierto el sitio, simplemente haz clic en Buscar mi iPhone, para que puedas rastrear tu teléfono en un mapa o alternativamente enviar un timbre, enviar un SMS desde tu teléfono con las coordenadas detectadas o borrar el contenido de la memoria.
Los detalles completos de esta función se encuentran en la guía sobre cómo encontrar su iPhone perdido o robado.
https://imeicolombia.net.co/verificar/